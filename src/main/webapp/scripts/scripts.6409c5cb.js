srbkWebApp = angular.module("srbkWebApp", ["ngAnimate", "ngCookies", "ngMessages", "ngResource", "ngRoute", "ngSanitize", "ngTouch"]), srbkWebApp.factory("moviesFactory", ["$resource", function (a) {
    return a("/rest/products")
}]), srbkWebApp.factory("seancesFactory", ["$resource", "$routeParams", function (a) {
    return{query: function (b) {
        return a("/rest/seances/:id", {id: b}).query()
    }}
}]), srbkWebApp.factory("seatsFactory", ["$resource", "$routeParams", function (a, b) {
    return{get: function () {
        return a("/rest/seances/onewithoccupation/:id", {id: b.sid}).get()
    }}
}]), srbkWebApp.factory("reservationsFactory", ["$resource", "$routeParams", function (a) {
    return{get: function (b) {
        return a("/rest/reservations/:id", {id: b}).get()
    }}
}]), srbkWebApp.config(["$routeProvider", function (a) {
    a.when("/", {templateUrl: "views/main.html", controller: "MainCtrl"}).when("/movie/:id", {templateUrl: "views/movie.html", controller: "movieDetailsCtrl"}).when("/bookTickets/:id", {templateUrl: "views/seances.html", controller: "bookTicketsController"}).when("/bookTickets/:id/:sid", {templateUrl: "views/seats.html", controller: "seatsChooseController"}).when("/bookTickets/:id/:sid/:prid", {templateUrl: "views/doreservation.html", controller: "reservationController"}).when("/bookTickets/:id/:sid/:prid/confirm", {templateUrl: "views/confirmreservation.html", controller: "reservationController"}).when("/bookTickets/:id/:sid/:prid/done", {templateUrl: "views/donereservation.html", controller: "reservationController"}).when("/about", {templateUrl: "views/about.html", controller: "AboutCtrl"}).when("/contact", {templateUrl: "views/contact.html", controller: "ContactCtrl"}).otherwise({redirectTo: "/"})
}]), srbkWebApp.controller("HeaderController", ["$scope", "$location", function (a, b) {
    a.isActive = function (a) {
        return a === b.path()
    }
}]), srbkWebApp.controller("MainCtrl", ["$scope", "moviesFactory", function (a, b) {
    a.movies = b.query(), a.getMovieById = function (b) {
        for (var c = a.movies, d = 0; d < c.length; d++) {
            var e = a.movies[d];
            e.id == b && (a.currMovie = e)
        }
    }
}]), srbkWebApp.controller("movieDetailsCtrl", ["$scope", "$routeParams", function (a, b) {
    a.getMovieById(b.id)
}]), srbkWebApp.controller("bookTicketsController", ["$scope", "$http", "$location", "$routeParams", "moviesFactory", "seancesFactory", function (a, b, c, d, e, f) {
    a.movies = e.query(), a.getMovieById(d.id), a.product_id = d.id, a.seances = f.query(d.id)
}]), srbkWebApp.controller("seatsChooseController", ["$scope", "$http", "$location", "$routeParams", "seatsFactory", function (a, b, c, d, e) {
    a.getMovieById(d.id), a.product_id = d.id, a.seanse_id = d.sid, a.seance = e.get(), a.selectedSeats = [], a.changeSeatStatus = function (b, c, d) {
        if (d.no) {
            var e = a.selectedSeats.indexOf(c);
            e > -1 && a.selectedSeats.splice(e, 1)
        } else a.selectedSeats.push(c)
    }, a.doReservation = function () {
        a.reservation = {}, a.reservation.seance = {id: a.seanse_id}, a.reservation.tickets = [], a.selectedSeats.forEach(function (b) {
            a.reservation.tickets.push({seat: {id: b}})
        }), b({method: "POST", url: "/rest/reservations/addPreliminaryReservation", data: JSON.stringify(a.reservation)}).success(function (b) {
            c.path("/bookTickets/" + a.product_id + "/" + a.seanse_id + "/" + b.id)
        }).error(function () {
            console.info("error")
        })
    }
}]), srbkWebApp.controller("reservationController", ["$scope", "$http", "$location", "$routeParams", "seatsFactory", "reservationsFactory", function (a, b, c, d, e, f) {
    a.getMovieById(d.id), a.seance = e.get(), a.reservation = f.get(d.prid), a.product_id = d.id, a.seanse_id = d.sid, a.reservation_id = d.prid, a.confirm = function () {
        a.reservation.reservationStatus = "FILLED", console.info(a.reservation), b({method: "POST", url: "/rest/reservations/saveReservation", data: angular.toJson(a.reservation)}).success(function (b) {
            c.path("/bookTickets/" + a.product_id + "/" + a.seanse_id + "/" + b.id + "/confirm")
        }).error(function () {
            console.info("errora")
        })
    }, a.finalConfirm = function () {
        a.reservation.reservationStatus = "CONFIRMED", console.info(a.reservation), b({method: "POST", url: "/rest/reservations/saveReservation", data: angular.toJson(a.reservation)}).success(function (b) {
            c.path("/bookTickets/" + a.product_id + "/" + a.seanse_id + "/" + b.id + "/done")
        }).error(function () {
            console.info("errora")
        })
    }
}]), srbkWebApp.controller("AboutCtrl", ["$scope", function (a) {
    a.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]
}]), srbkWebApp.controller("ContactCtrl", ["$scope", function (a) {
    a.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]
}]);