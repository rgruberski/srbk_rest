srbkWebApp = angular.module("srbkWebApp", ["ngAnimate", "ngCookies", "ngMessages", "ngResource", "ngRoute", "ngSanitize", "ngTouch"]), srbkWebApp.factory("moviesFactory", ["$resource", function (a) {
    return a("/rest/products")
}]), srbkWebApp.factory("seancesFactory", ["$resource", "$routeParams", function (a) {
    return{query: function (b) {
        return a("/rest/seances/:id", {id: b}).query()
    }}
}]), srbkWebApp.factory("seatsFactory", ["$resource", "$routeParams", function (a, b) {
    return{get: function () {
        return a("/rest/seances/onewithoccupation/:id", {id: b.sid}).get()
    }}
}]), srbkWebApp.factory("reservationsFactory", ["$resource", "$routeParams", "$rootScope", "$location", function (a, b, c, d) {
    return{get: function (b) {
        return a("/rest/reservations/:id", {id: b}).get(function () {
        }, function (a) {
            a.data.indexOf("ReservationExpiredException") > -1 && bootbox.alert("Wybrana rezerwacja jest nieważna. Proszę spróbować ponownie", function () {
                c.$apply(function () {
                    d.path("/")
                })
            }), a.data.indexOf("NullPointerException") > -1 && bootbox.alert("Wybrana rezerwacja nie została znaleziona. Proszę spróbować ponownie", function () {
                c.$apply(function () {
                    d.path("/")
                })
            })
        })
    }}
}]), $, srbkWebApp.config(["$routeProvider", function (a) {
    a.when("/", {templateUrl: "views/main.html", controller: "MainCtrl"}).when("/movie/:id", {templateUrl: "views/movie.html", controller: "movieDetailsCtrl"}).when("/bookTickets/:id", {templateUrl: "views/seances.html", controller: "bookTicketsController"}).when("/bookTickets/:id/:sid", {templateUrl: "views/seats.html", controller: "seatsChooseController"}).when("/bookTickets/:id/:sid/:prid", {templateUrl: "views/doreservation.html", controller: "reservationController"}).when("/bookTickets/:id/:sid/:prid/confirm", {templateUrl: "views/confirmreservation.html", controller: "reservationController"}).when("/bookTickets/:id/:sid/:prid/done", {templateUrl: "views/donereservation.html", controller: "reservationController"}).when("/about", {templateUrl: "views/about.html", controller: "AboutCtrl"}).when("/contact", {templateUrl: "views/contact.html", controller: "ContactCtrl"}).otherwise({redirectTo: "/"})
}]), srbkWebApp.controller("HeaderController", ["$scope", "$location", function (a, b) {
    a.isActive = function (a) {
        return a === b.path()
    }
}]), srbkWebApp.controller("MainCtrl", ["$scope", "moviesFactory", function (a, b) {
    a.movies = b.query(), a.getMovieById = function (b) {
        for (var c = a.movies, d = 0; d < c.length; d++) {
            var e = a.movies[d];
            e.id == b && (a.currMovie = e)
        }
    }
}]), srbkWebApp.controller("movieDetailsCtrl", ["$scope", "$routeParams", function (a, b) {
    a.getMovieById(b.id)
}]), srbkWebApp.controller("bookTicketsController", ["$scope", "$http", "$location", "$routeParams", "moviesFactory", "seancesFactory", function (a, b, c, d, e, f) {
    a.movies = e.query(), a.getMovieById(d.id), a.product_id = d.id, a.seances = f.query(d.id)
}]), srbkWebApp.controller("seatsChooseController", ["$scope", "$http", "$location", "$routeParams", "seatsFactory", function (a, b, c, d, e) {
    a.getMovieById(d.id), a.product_id = d.id, a.seanse_id = d.sid, a.seance = e.get(), a.selectedSeats = [], a.changeSeatStatus = function (b, c, d) {
        if (d.no) {
            var e = a.selectedSeats.indexOf(c);
            e > -1 && a.selectedSeats.splice(e, 1)
        } else a.selectedSeats.push(c)
    }, a.doReservation = function () {
        a.reservation = {}, a.reservation.seance = {id: a.seanse_id}, a.reservation.tickets = [], a.selectedSeats.forEach(function (b) {
            a.reservation.tickets.push({seat: {id: b}})
        }), b({method: "POST", url: "/rest/reservations/addPreliminaryReservation", data: JSON.stringify(a.reservation)}).success(function (b) {
            c.path("/bookTickets/" + a.product_id + "/" + a.seanse_id + "/" + b.id)
        }).error(function (b) {
            b.indexOf("ReservationBusySeatException") > -1 && bootbox.alert("Niestety wybrane miejsca zostały zarezerwowane przez kogoś innego. Proszę wybrać inne miejsca.", function () {
                a.selectedSeats = [], a.seance = e.get()
            })
        })
    }
}]), srbkWebApp.controller("reservationController", ["$scope", "$http", "$location", "$routeParams", "seatsFactory", "reservationsFactory", function (a, b, c, d, e, f) {
    a.getMovieById(d.id), a.seance = e.get(), a.reservation = f.get(d.prid), console.info(a.reservation), a.product_id = d.id, a.seanse_id = d.sid, a.reservation_id = d.prid, a.confirm = function () {
        a.reservation.reservationStatus = "FILLED", b({method: "POST", url: "/rest/reservations/saveReservation", data: angular.toJson(a.reservation)}).success(function (b) {
            c.path("/bookTickets/" + a.product_id + "/" + a.seanse_id + "/" + b.id + "/confirm")
        }).error(function () {
            bootbox.alert("Wystąpił problem podczas składania rezerwacji, program przerywa prace. Proszę spróbować ponownie", function () {
                c.path("/")
            })
        })
    }, a.finalConfirm = function () {
        a.reservation.reservationStatus = "CONFIRMED", b({method: "POST", url: "/rest/reservations/saveReservation", data: angular.toJson(a.reservation)}).success(function (b) {
            c.path("/bookTickets/" + a.product_id + "/" + a.seanse_id + "/" + b.id + "/done")
        }).error(function () {
            bootbox.alert("Wystąpił problem podczas składania rezerwacji, program przerywa prace. Proszę spróbować ponownie", function () {
                c.path("/")
            })
        })
    }
}]), srbkWebApp.controller("AboutCtrl", ["$scope", function (a) {
    a.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]
}]), srbkWebApp.controller("ContactCtrl", ["$scope", function (a) {
    a.awesomeThings = ["HTML5 Boilerplate", "AngularJS", "Karma"]
}]);