
package com.example.srbk.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reservationAuthorizationMethod.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="reservationAuthorizationMethod">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="SMS"/>
 *     &lt;enumeration value="EMAIL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "reservationAuthorizationMethod")
@XmlEnum
public enum ReservationAuthorizationMethod {

    SMS,
    EMAIL;

    public static ReservationAuthorizationMethod fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
