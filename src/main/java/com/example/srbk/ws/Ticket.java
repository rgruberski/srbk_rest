
package com.example.srbk.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ticket complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="ticket">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="type" type="{http://ws.srbk.example.com/}ticketType" minOccurs="0"/>
 *         &lt;element name="seat" type="{http://ws.srbk.example.com/}seat" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ticket", propOrder = {
        "id",
        "type",
        "seat"
})
public class Ticket {

    protected Integer id;
    protected TicketType type;
    protected Seat seat;

    /**
     * Gets the value of the id property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the type property.
     *
     * @return possible object is
     * {@link TicketType }
     */
    public TicketType getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     *
     * @param value allowed object is
     *              {@link TicketType }
     */
    public void setType(TicketType value) {
        this.type = value;
    }

    /**
     * Gets the value of the seat property.
     *
     * @return possible object is
     * {@link Seat }
     */
    public Seat getSeat() {
        return seat;
    }

    /**
     * Sets the value of the seat property.
     *
     * @param value allowed object is
     *              {@link Seat }
     */
    public void setSeat(Seat value) {
        this.seat = value;
    }

}
