
package com.example.srbk.ws;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reservationStatus.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;simpleType name="reservationStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="CANCELED"/>
 *     &lt;enumeration value="PRELIMINARY"/>
 *     &lt;enumeration value="FILLED"/>
 *     &lt;enumeration value="CONFIRMED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 */
@XmlType(name = "reservationStatus")
@XmlEnum
public enum ReservationStatus {

    CANCELED,
    PRELIMINARY,
    FILLED,
    CONFIRMED;

    public static ReservationStatus fromValue(String v) {
        return valueOf(v);
    }

    public String value() {
        return name();
    }

}
