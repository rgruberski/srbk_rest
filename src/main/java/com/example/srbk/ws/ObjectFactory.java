
package com.example.srbk.ws;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.example.srbk.ws package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ReservationBusySeatException_QNAME = new QName("http://ws.srbk.example.com/", "ReservationBusySeatException");
    private final static QName _Seat_QNAME = new QName("http://ws.srbk.example.com/", "seat");
    private final static QName _ReservationExpiredException_QNAME = new QName("http://ws.srbk.example.com/", "ReservationExpiredException");
    private final static QName _GetReservationResponse_QNAME = new QName("http://ws.srbk.example.com/", "getReservationResponse");
    private final static QName _SaveReservation_QNAME = new QName("http://ws.srbk.example.com/", "saveReservation");
    private final static QName _AddPreliminaryReservation_QNAME = new QName("http://ws.srbk.example.com/", "addPreliminaryReservation");
    private final static QName _GetReservation_QNAME = new QName("http://ws.srbk.example.com/", "getReservation");
    private final static QName _Product_QNAME = new QName("http://ws.srbk.example.com/", "product");
    private final static QName _ReservationNotExistsException_QNAME = new QName("http://ws.srbk.example.com/", "ReservationNotExistsException");
    private final static QName _AddPreliminaryReservationResponse_QNAME = new QName("http://ws.srbk.example.com/", "addPreliminaryReservationResponse");
    private final static QName _Ticket_QNAME = new QName("http://ws.srbk.example.com/", "ticket");
    private final static QName _Hall_QNAME = new QName("http://ws.srbk.example.com/", "hall");
    private final static QName _SaveReservationResponse_QNAME = new QName("http://ws.srbk.example.com/", "saveReservationResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.example.srbk.ws
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ReservationNotExistsException }
     */
    public ReservationNotExistsException createReservationNotExistsException() {
        return new ReservationNotExistsException();
    }

    /**
     * Create an instance of {@link AddPreliminaryReservationResponse }
     */
    public AddPreliminaryReservationResponse createAddPreliminaryReservationResponse() {
        return new AddPreliminaryReservationResponse();
    }

    /**
     * Create an instance of {@link Product }
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link Ticket }
     */
    public Ticket createTicket() {
        return new Ticket();
    }

    /**
     * Create an instance of {@link Hall }
     */
    public Hall createHall() {
        return new Hall();
    }

    /**
     * Create an instance of {@link SaveReservationResponse }
     */
    public SaveReservationResponse createSaveReservationResponse() {
        return new SaveReservationResponse();
    }

    /**
     * Create an instance of {@link ReservationBusySeatException }
     */
    public ReservationBusySeatException createReservationBusySeatException() {
        return new ReservationBusySeatException();
    }

    /**
     * Create an instance of {@link Seat }
     */
    public Seat createSeat() {
        return new Seat();
    }

    /**
     * Create an instance of {@link ReservationExpiredException }
     */
    public ReservationExpiredException createReservationExpiredException() {
        return new ReservationExpiredException();
    }

    /**
     * Create an instance of {@link GetReservationResponse }
     */
    public GetReservationResponse createGetReservationResponse() {
        return new GetReservationResponse();
    }

    /**
     * Create an instance of {@link SaveReservation }
     */
    public SaveReservation createSaveReservation() {
        return new SaveReservation();
    }

    /**
     * Create an instance of {@link GetReservation }
     */
    public GetReservation createGetReservation() {
        return new GetReservation();
    }

    /**
     * Create an instance of {@link AddPreliminaryReservation }
     */
    public AddPreliminaryReservation createAddPreliminaryReservation() {
        return new AddPreliminaryReservation();
    }

    /**
     * Create an instance of {@link TicketType }
     */
    public TicketType createTicketType() {
        return new TicketType();
    }

    /**
     * Create an instance of {@link ProductCategory }
     */
    public ProductCategory createProductCategory() {
        return new ProductCategory();
    }

    /**
     * Create an instance of {@link SeatsRow }
     */
    public SeatsRow createSeatsRow() {
        return new SeatsRow();
    }

    /**
     * Create an instance of {@link Reservation }
     */
    public Reservation createReservation() {
        return new Reservation();
    }

    /**
     * Create an instance of {@link Seance }
     */
    public Seance createSeance() {
        return new Seance();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationBusySeatException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "ReservationBusySeatException")
    public JAXBElement<ReservationBusySeatException> createReservationBusySeatException(ReservationBusySeatException value) {
        return new JAXBElement<ReservationBusySeatException>(_ReservationBusySeatException_QNAME, ReservationBusySeatException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Seat }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "seat")
    public JAXBElement<Seat> createSeat(Seat value) {
        return new JAXBElement<Seat>(_Seat_QNAME, Seat.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationExpiredException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "ReservationExpiredException")
    public JAXBElement<ReservationExpiredException> createReservationExpiredException(ReservationExpiredException value) {
        return new JAXBElement<ReservationExpiredException>(_ReservationExpiredException_QNAME, ReservationExpiredException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReservationResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "getReservationResponse")
    public JAXBElement<GetReservationResponse> createGetReservationResponse(GetReservationResponse value) {
        return new JAXBElement<GetReservationResponse>(_GetReservationResponse_QNAME, GetReservationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveReservation }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "saveReservation")
    public JAXBElement<SaveReservation> createSaveReservation(SaveReservation value) {
        return new JAXBElement<SaveReservation>(_SaveReservation_QNAME, SaveReservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPreliminaryReservation }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "addPreliminaryReservation")
    public JAXBElement<AddPreliminaryReservation> createAddPreliminaryReservation(AddPreliminaryReservation value) {
        return new JAXBElement<AddPreliminaryReservation>(_AddPreliminaryReservation_QNAME, AddPreliminaryReservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetReservation }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "getReservation")
    public JAXBElement<GetReservation> createGetReservation(GetReservation value) {
        return new JAXBElement<GetReservation>(_GetReservation_QNAME, GetReservation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Product }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "product")
    public JAXBElement<Product> createProduct(Product value) {
        return new JAXBElement<Product>(_Product_QNAME, Product.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ReservationNotExistsException }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "ReservationNotExistsException")
    public JAXBElement<ReservationNotExistsException> createReservationNotExistsException(ReservationNotExistsException value) {
        return new JAXBElement<ReservationNotExistsException>(_ReservationNotExistsException_QNAME, ReservationNotExistsException.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddPreliminaryReservationResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "addPreliminaryReservationResponse")
    public JAXBElement<AddPreliminaryReservationResponse> createAddPreliminaryReservationResponse(AddPreliminaryReservationResponse value) {
        return new JAXBElement<AddPreliminaryReservationResponse>(_AddPreliminaryReservationResponse_QNAME, AddPreliminaryReservationResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ticket }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "ticket")
    public JAXBElement<Ticket> createTicket(Ticket value) {
        return new JAXBElement<Ticket>(_Ticket_QNAME, Ticket.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Hall }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "hall")
    public JAXBElement<Hall> createHall(Hall value) {
        return new JAXBElement<Hall>(_Hall_QNAME, Hall.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SaveReservationResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://ws.srbk.example.com/", name = "saveReservationResponse")
    public JAXBElement<SaveReservationResponse> createSaveReservationResponse(SaveReservationResponse value) {
        return new JAXBElement<SaveReservationResponse>(_SaveReservationResponse_QNAME, SaveReservationResponse.class, null, value);
    }

}
