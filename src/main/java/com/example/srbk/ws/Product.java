
package com.example.srbk.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for product complex type.
 * <p/>
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p/>
 * <pre>
 * &lt;complexType name="product">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="category" type="{http://ws.srbk.example.com/}productCategory" minOccurs="0"/>
 *         &lt;element name="status" type="{http://ws.srbk.example.com/}productStatus" minOccurs="0"/>
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="picture" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ageRestriction" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="updateTime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "product", propOrder = {
        "id",
        "category",
        "status",
        "name",
        "description",
        "picture",
        "ageRestriction",
        "updateTime"
})
public class Product {

    protected Integer id;
    protected ProductCategory category;
    @XmlSchemaType(name = "string")
    protected ProductStatus status;
    protected String name;
    protected String description;
    protected String picture;
    protected Integer ageRestriction;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar updateTime;

    /**
     * Gets the value of the id property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets the value of the id property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Gets the value of the category property.
     *
     * @return possible object is
     * {@link ProductCategory }
     */
    public ProductCategory getCategory() {
        return category;
    }

    /**
     * Sets the value of the category property.
     *
     * @param value allowed object is
     *              {@link ProductCategory }
     */
    public void setCategory(ProductCategory value) {
        this.category = value;
    }

    /**
     * Gets the value of the status property.
     *
     * @return possible object is
     * {@link ProductStatus }
     */
    public ProductStatus getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     *
     * @param value allowed object is
     *              {@link ProductStatus }
     */
    public void setStatus(ProductStatus value) {
        this.status = value;
    }

    /**
     * Gets the value of the name property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the description property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the picture property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getPicture() {
        return picture;
    }

    /**
     * Sets the value of the picture property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setPicture(String value) {
        this.picture = value;
    }

    /**
     * Gets the value of the ageRestriction property.
     *
     * @return possible object is
     * {@link Integer }
     */
    public Integer getAgeRestriction() {
        return ageRestriction;
    }

    /**
     * Sets the value of the ageRestriction property.
     *
     * @param value allowed object is
     *              {@link Integer }
     */
    public void setAgeRestriction(Integer value) {
        this.ageRestriction = value;
    }

    /**
     * Gets the value of the updateTime property.
     *
     * @return possible object is
     * {@link XMLGregorianCalendar }
     */
    public XMLGregorianCalendar getUpdateTime() {
        return updateTime;
    }

    /**
     * Sets the value of the updateTime property.
     *
     * @param value allowed object is
     *              {@link XMLGregorianCalendar }
     */
    public void setUpdateTime(XMLGregorianCalendar value) {
        this.updateTime = value;
    }

}
