package com.example.srbk.service;

import com.example.srbk.model.ProductCategory;

import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
public interface ProductCategoryService {
    public List<ProductCategory> fetchAll();
}
