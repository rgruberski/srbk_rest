package com.example.srbk.service;

import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.exception.ReservationExpiredException;
import com.example.srbk.exception.ReservationNotExistsException;
import com.example.srbk.model.Reservation;

/**
 * Created by robert on 15.01.15.
 */
public interface ReservationService {
    public Reservation fetchReservation(Integer id) throws ReservationExpiredException, ReservationNotExistsException;

    public Reservation addPreliminaryReservation(Reservation reservation) throws ReservationBusySeatException;

    public Reservation saveReservation(Reservation reservation);
}
