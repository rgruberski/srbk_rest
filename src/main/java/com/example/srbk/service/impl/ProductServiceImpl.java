package com.example.srbk.service.impl;

import com.example.srbk.dao.ConfigDao;
import com.example.srbk.dao.ProductDao;
import com.example.srbk.enumerator.ProductStatus;
import com.example.srbk.model.Config;
import com.example.srbk.model.Product;
import com.example.srbk.model.ProductCategory;
import com.example.srbk.service.ProductService;
import com.example.srbk.ws.ProductService_Service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.datatype.DatatypeFactory;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by mateusz on 11.01.15.
 */
@Named
@RequestScoped
public class ProductServiceImpl implements ProductService {

    @Inject
    private ProductDao productDao;

    @Inject
    private ConfigDao configDao;

    @Override
    public List<Product> fetchAll() {
        return productDao.findAll();
    }

    @Override
    public void importFromWebService() throws Exception {

        com.example.srbk.ws.ProductService service = new ProductService_Service().getProductServiceImplPort();

        Date updateDate = new Date(0);

        try {
            updateDate = configDao.getDate("productSynchronizationTime");
        } catch (Exception e) {

        }

        GregorianCalendar updateDateGregorianCalendar = new GregorianCalendar();
        updateDateGregorianCalendar.setTime(updateDate);

        List<com.example.srbk.ws.Product> products = service.fetchAllProductsUpdatedAfterDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(updateDateGregorianCalendar));

        for (com.example.srbk.ws.Product p : products) {

            productDao.transactionOpen();

            Product product = new Product();

            product.setId(p.getId());
            product.setCategory(new ProductCategory(p.getCategory().getId(), p.getCategory().getName()));
            product.setStatus(ProductStatus.valueOf(p.getStatus().name()));
            product.setName(p.getName());
            product.setDescription(p.getDescription());
            product.setPicture(p.getPicture());
            product.setAgeRestriction(p.getAgeRestriction());

            productDao.update(product);
            productDao.transactionCommit();
        }

        Config productSynchronizationTimeConfig = configDao.getConfigByParameter("productSynchronizationTime");

        if (productSynchronizationTimeConfig == null) {
            productSynchronizationTimeConfig = new Config();
            productSynchronizationTimeConfig.setParameter("productSynchronizationTime");
        }

        productSynchronizationTimeConfig.setDateValue(new Date());

        configDao.transactionOpen();
        configDao.update(productSynchronizationTimeConfig);
        configDao.transactionCommit();
    }
}
