package com.example.srbk.service.impl;

import com.example.srbk.dao.ConfigDao;
import com.example.srbk.dao.ProductDao;
import com.example.srbk.dao.SeanceDao;
import com.example.srbk.model.*;
import com.example.srbk.service.SeanceService;
import com.example.srbk.ws.SeanceService_Service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.xml.datatype.DatatypeFactory;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
@Named
@RequestScoped
public class SeanceServiceImpl implements SeanceService {

    @Inject
    private SeanceDao seanceDao;

    @Inject
    private ProductDao productDao;

    @Inject
    private ConfigDao configDao;

    @Override
    public List<Seance> fetchAll() {
        return seanceDao.findAll();
    }

    @Override
    public List<Seance> fetchProductSeances(Integer product_id) {
        return seanceDao.findProductSeances(productDao.find(product_id));
    }

    @Override
    public Seance fetchSeanceWithOccupation(Integer seance_id) {

        Seance seance = seanceDao.find(seance_id);

        com.example.srbk.ws.SeanceService service = new SeanceService_Service().getSeanceServiceImplPort();
        com.example.srbk.ws.Seance remoteSeance = service.getSeanceWithOccupation(seance_id);

        for (com.example.srbk.ws.SeatsRow rsr : remoteSeance.getHall().getSeatsRowsWithOccupation()) {

            SeatsRow seatsRow = new SeatsRow(rsr.getSymbol());

            for (com.example.srbk.ws.Seat rs : rsr.getSeatList()) {
                seatsRow.getSeatList().add(new Seat(rs.getId(), rs.getRow(), rs.getNumber(), rs.isOccupied()));
            }

            seance.getHall().getSeatsRows().add(seatsRow);
        }

        return seance;
    }

    @Override
    public void importFromWebService() throws Exception {

        com.example.srbk.ws.SeanceService service = new SeanceService_Service().getSeanceServiceImplPort();

        Date updateDate = new Date(0);

        try {
            updateDate = configDao.getDate("seanceSynchronizationTime");
        } catch (Exception e) {

        }

        GregorianCalendar updateDateGregorianCalendar = new GregorianCalendar();
        updateDateGregorianCalendar.setTime(updateDate);

        List<com.example.srbk.ws.Seance> seances = service.fetchAllSeancesUpdatedAfterDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(updateDateGregorianCalendar));

        for (com.example.srbk.ws.Seance s : seances) {

            seanceDao.transactionOpen();

            Seance seance = new Seance();

            seance.setId(s.getId());
            seance.setProduct(new Product(s.getProduct().getId()));
            seance.setHall(new Hall(s.getHall().getId(), s.getHall().getName()));
            seance.setStartTime(s.getStartTime().toGregorianCalendar().getTime());

            seanceDao.update(seance);
            seanceDao.transactionCommit();
        }

        Config seanceSynchronizationTimeConfig = configDao.getConfigByParameter("seanceSynchronizationTime");

        if (seanceSynchronizationTimeConfig == null) {
            seanceSynchronizationTimeConfig = new Config();
            seanceSynchronizationTimeConfig.setParameter("seanceSynchronizationTime");
        }

        seanceSynchronizationTimeConfig.setDateValue(new Date());

        configDao.transactionOpen();
        configDao.update(seanceSynchronizationTimeConfig);
        configDao.transactionCommit();
    }
}
