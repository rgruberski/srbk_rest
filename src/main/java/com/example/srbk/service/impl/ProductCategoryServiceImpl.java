package com.example.srbk.service.impl;

import com.example.srbk.dao.ProductCategoryDao;
import com.example.srbk.model.ProductCategory;
import com.example.srbk.service.ProductCategoryService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
@Named
@RequestScoped
public class ProductCategoryServiceImpl implements ProductCategoryService {

    @Inject
    ProductCategoryDao productCategoryDao;

    @Override
    public List<ProductCategory> fetchAll() {
        return productCategoryDao.findAll();
    }
}
