package com.example.srbk.service.impl;

import com.example.srbk.enumerator.ReservationAuthorizationMethod;
import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.exception.ReservationExpiredException;
import com.example.srbk.exception.ReservationNotExistsException;
import com.example.srbk.model.*;
import com.example.srbk.service.ReservationService;
import com.example.srbk.ws.ReservationBusySeatException_Exception;
import com.example.srbk.ws.ReservationExpiredException_Exception;
import com.example.srbk.ws.ReservationNotExistsException_Exception;
import com.example.srbk.ws.ReservationsService_Service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by robert on 15.01.15.
 */
@Named
@RequestScoped
public class ReservationServiceImpl implements ReservationService {

    @Override
    public Reservation fetchReservation(Integer id) throws ReservationExpiredException, ReservationNotExistsException {

        com.example.srbk.ws.ReservationsService service = new ReservationsService_Service().getReservationsServiceImplPort();

        com.example.srbk.ws.Reservation remoteReservation = null;
        try {
            remoteReservation = service.getReservation(id);
        } catch (ReservationExpiredException_Exception e) {
            throw new ReservationExpiredException();
        } catch (ReservationNotExistsException_Exception e) {
            throw new ReservationNotExistsException();
        }

        Reservation reservation = new Reservation();
        Seance seance = new Seance();

        seance.setId(remoteReservation.getSeance().getId());

        reservation.setId(remoteReservation.getId());
        reservation.setSeance(seance);

        reservation.setReservationStatus(com.example.srbk.enumerator.ReservationStatus.valueOf(remoteReservation.getStatus().name()));

        if (remoteReservation.getAuthorizationMethod() != null)
            reservation.setAuthorizationMethod(ReservationAuthorizationMethod.valueOf(remoteReservation.getAuthorizationMethod().name()));

        reservation.setName(remoteReservation.getName());
        reservation.setPhone(remoteReservation.getPhone());
        reservation.setEmail(remoteReservation.getEmail());
        reservation.setExpirationDate(remoteReservation.getExpirationDate().toGregorianCalendar().getTime());

        List<Ticket> tickets = new ArrayList<Ticket>();

        for (com.example.srbk.ws.Ticket remoteTicket : remoteReservation.getTickets()) {

            Ticket ticket = new Ticket(remoteTicket.getId());
            Seat seat = new Seat(remoteTicket.getSeat().getId(), remoteTicket.getSeat().getRow(), remoteTicket.getSeat().getNumber(), remoteTicket.getSeat().isOccupied());

            if (remoteTicket.getType() != null) {
                TicketType ticketType = new TicketType(remoteTicket.getType().getId());
                ticket.setType(ticketType);
            }

            ticket.setSeat(seat);
            tickets.add(ticket);
        }

        reservation.setTickets(tickets);

        return reservation;
    }

    @Override
    public Reservation addPreliminaryReservation(Reservation reservation) throws ReservationBusySeatException {

        com.example.srbk.ws.ReservationsService service = new ReservationsService_Service().getReservationsServiceImplPort();

        com.example.srbk.ws.Reservation remoteReservation = new com.example.srbk.ws.Reservation();
        com.example.srbk.ws.Seance remoteSeance = new com.example.srbk.ws.Seance();

        remoteSeance.setId(reservation.getSeance().getId());

        remoteReservation.setSeance(remoteSeance);

        for (Ticket ticket : reservation.getTickets()) {

            com.example.srbk.ws.Ticket remoteTicket = new com.example.srbk.ws.Ticket();
            com.example.srbk.ws.Seat remoteSeat = new com.example.srbk.ws.Seat();

            remoteSeat.setId(ticket.getSeat().getId());
            remoteTicket.setSeat(remoteSeat);

            remoteReservation.getTickets().add(remoteTicket);
        }

        try {
            remoteReservation = service.addPreliminaryReservation(remoteReservation);
        } catch (ReservationBusySeatException_Exception e) {
            throw new ReservationBusySeatException();
        }

        reservation.setId(remoteReservation.getId());

        return reservation;
    }

    @Override
    public Reservation saveReservation(Reservation reservation) {

        com.example.srbk.ws.ReservationsService service = new ReservationsService_Service().getReservationsServiceImplPort();

        com.example.srbk.ws.Reservation remoteReservation = new com.example.srbk.ws.Reservation();
        com.example.srbk.ws.Seance remoteSeance = new com.example.srbk.ws.Seance();

        remoteSeance.setId(reservation.getSeance().getId());

        remoteReservation.setSeance(remoteSeance);

        remoteReservation.setId(reservation.getId());
        remoteReservation.setName(reservation.getName());
        remoteReservation.setEmail(reservation.getEmail());
        remoteReservation.setPhone(reservation.getPhone());

        GregorianCalendar expirationDateGregorianCalendar = new GregorianCalendar();
        expirationDateGregorianCalendar.setTime(reservation.getExpirationDate());

        try {
            remoteReservation.setExpirationDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(expirationDateGregorianCalendar));
        } catch (DatatypeConfigurationException e) {
            e.printStackTrace();
        }
        ;

        remoteReservation.setAuthorizationMethod(com.example.srbk.ws.ReservationAuthorizationMethod.fromValue(reservation.getAuthorizationMethod().name()));

        remoteReservation.setStatus(com.example.srbk.ws.ReservationStatus.fromValue(reservation.getReservationStatus().name()));

        for (Ticket ticket : reservation.getTickets()) {

            com.example.srbk.ws.Ticket remoteTicket = new com.example.srbk.ws.Ticket();
            com.example.srbk.ws.TicketType remoteTicketType = new com.example.srbk.ws.TicketType();
            com.example.srbk.ws.Seat remoteSeat = new com.example.srbk.ws.Seat();

            remoteSeat.setId(ticket.getSeat().getId());
            remoteTicketType.setId(ticket.getType().getId());

            remoteTicket.setId(ticket.getId());
            remoteTicket.setSeat(remoteSeat);
            remoteTicket.setType(remoteTicketType);

            remoteReservation.getTickets().add(remoteTicket);
        }

        remoteReservation = service.saveReservation(remoteReservation);

        reservation.setId(remoteReservation.getId());

        return reservation;
    }
}
