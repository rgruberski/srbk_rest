package com.example.srbk.service;

import com.example.srbk.model.Seance;

import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
public interface SeanceService {
    public List<Seance> fetchAll();
    public List<Seance> fetchProductSeances(Integer product_id);

    public Seance fetchSeanceWithOccupation(Integer seance_id);
    public void importFromWebService() throws Exception;
}
