package com.example.srbk.service;

import com.example.srbk.model.Product;

import java.util.List;

/**
 * Created by mateusz on 11.01.15.
 */

public interface ProductService {
    public List<Product> fetchAll();

    public void importFromWebService() throws Exception;
}
