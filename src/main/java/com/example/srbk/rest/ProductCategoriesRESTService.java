package com.example.srbk.rest;

import com.example.srbk.model.ProductCategory;
import com.example.srbk.service.ProductCategoryService;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@ManagedBean
@RequestScoped
@Path("/categories")
public class ProductCategoriesRESTService {

    @Inject
    private ProductCategoryService productCategoryService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public List<ProductCategory> list() {
        return productCategoryService.fetchAll();
    }
}
