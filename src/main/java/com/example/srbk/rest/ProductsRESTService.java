package com.example.srbk.rest;

import com.example.srbk.model.Product;
import com.example.srbk.service.ProductService;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by robert on 14.01.15.
 */
@ManagedBean
@RequestScoped
@Path("/products")
public class ProductsRESTService {

    @Inject
    private ProductService productService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/")
    public List<Product> list() {
        return productService.fetchAll();
    }
}
