package com.example.srbk.rest;

import com.example.srbk.exception.ReservationBusySeatException;
import com.example.srbk.exception.ReservationExpiredException;
import com.example.srbk.exception.ReservationNotExistsException;
import com.example.srbk.model.Reservation;
import com.example.srbk.service.ReservationService;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * Created by robert on 15.01.15.
 */
@ManagedBean
@RequestScoped
@Path("/reservations")
public class ReservationsRESTSevice {

    @Inject
    private ReservationService reservationService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{reservation_id}")
    public Reservation fetchReservation(@PathParam("reservation_id") Integer reservation_id) throws ReservationNotExistsException, ReservationExpiredException {
        return reservationService.fetchReservation(reservation_id);
    }

    @POST
    @Path("/addPreliminaryReservation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Reservation addPreliminaryReservation(Reservation reservation) throws ReservationBusySeatException {

        reservationService.addPreliminaryReservation(reservation);

        return reservation;
    }

    @POST
    @Path("/saveReservation")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Reservation saveReservation(Reservation reservation) {

        reservationService.saveReservation(reservation);

        return reservation;
    }
}
