package com.example.srbk.rest;

import com.example.srbk.model.Seance;
import com.example.srbk.service.SeanceService;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
@ManagedBean
@RequestScoped
@Path("/seances")
public class SeanceRESTService {

    @Inject
    private SeanceService seanceService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{product_id}")
    public List<Seance> list(@PathParam("product_id") Integer product_id) {
        return seanceService.fetchProductSeances(product_id);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/onewithoccupation/{seance_id}")
    public Seance getSeanceWithOccupation(@PathParam("seance_id") Integer seance_id) {
        return seanceService.fetchSeanceWithOccupation(seance_id);
    }


}
