package com.example.srbk.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "product_categories")
public class ProductCategory implements Serializable {

    @Id
    private Integer id;

    private String name;

    public ProductCategory() {

    }

    public ProductCategory(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
