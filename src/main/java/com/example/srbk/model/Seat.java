package com.example.srbk.model;

/**
 * Created by robert on 15.01.15.
 */
public class Seat {

    private int id;
    private String row;
    private int number;
    private boolean occupied;

    public Seat() {
    }

    public Seat(int id, String row, int number, boolean occupied) {
        this.id = id;
        this.row = row;
        this.number = number;
        this.occupied = occupied;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public boolean isOccupied() {
        return occupied;
    }

    public void setOccupied(boolean occupied) {
        this.occupied = occupied;
    }
}
