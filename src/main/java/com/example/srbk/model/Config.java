package com.example.srbk.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by mateusz on 14.01.15.
 */
@Entity
@Table(name = "config")
public class Config {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(nullable = false)
    private String parameter;

    @Column(nullable = true)
    private Date dateValue;

    public Config() {

    }

    public Config(Date dateValue) {
        this.dateValue = dateValue;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDateValue() {
        return dateValue;
    }

    public void setDateValue(Date dateValue) {
        this.dateValue = dateValue;
    }

    public String getParameter() {
        return parameter;
    }

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
}