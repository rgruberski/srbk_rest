package com.example.srbk.model;

import com.example.srbk.enumerator.ReservationAuthorizationMethod;
import com.example.srbk.enumerator.ReservationStatus;

import java.util.Date;
import java.util.List;

/**
 * Created by robert on 15.01.15.
 */

public class Reservation {

    private Integer id;

    private Date date = new Date();

    private Seance seance;
    private ReservationStatus reservationStatus;

    private ReservationAuthorizationMethod authorizationMethod;

    private List<Ticket> tickets;

    private String name;
    private String phone;
    private String email;

    private Date expirationDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Seance getSeance() {
        return seance;
    }

    public void setSeance(Seance seance) {
        this.seance = seance;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    public ReservationAuthorizationMethod getAuthorizationMethod() {
        return authorizationMethod;
    }

    public void setAuthorizationMethod(ReservationAuthorizationMethod authorizationMethod) {
        this.authorizationMethod = authorizationMethod;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public void setTickets(List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
}
