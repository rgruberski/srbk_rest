package com.example.srbk.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by robert on 15.01.15.
 */
@Entity
@Table(name = "halls")
public class Hall implements Serializable {

    @Id
    private Integer id;

    @Transient
    private List<SeatsRow> seatsRows = new ArrayList<SeatsRow>();

    private String name;

    public Hall() {
    }

    public Hall(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<SeatsRow> getSeatsRows() {
        return seatsRows;
    }

    public void setSeatsRows(List<SeatsRow> seatsRows) {
        this.seatsRows = seatsRows;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
