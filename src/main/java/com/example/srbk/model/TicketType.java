package com.example.srbk.model;

/**
 * Created by robert on 16.01.15.
 */
public class TicketType {

    private Integer id;

    public TicketType() {

    }

    public TicketType(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
