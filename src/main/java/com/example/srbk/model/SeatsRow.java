package com.example.srbk.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by robert on 15.01.15.
 */
public class SeatsRow {

    private List<Seat> seatList = new ArrayList<Seat>();
    private String symbol;

    public SeatsRow() {
    }

    public SeatsRow(String symbol) {
        this.symbol = symbol;
    }

    public List<Seat> getSeatList() {
        return seatList;
    }

    public void setSeatList(List<Seat> seatList) {
        this.seatList = seatList;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }
}
