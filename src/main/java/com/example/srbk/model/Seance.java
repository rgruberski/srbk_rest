package com.example.srbk.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "seances")
public class Seance implements Serializable {

    @Id
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "hall_id")
    private Hall hall;

    private Date startTime;

    public Seance() {
    }

    public Seance(Integer id, Hall hall, Date startTime) {
        this.id = id;
        this.hall = hall;
        this.startTime = startTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Hall getHall() {
        return hall;
    }

    public void setHall(Hall hall) {
        this.hall = hall;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
