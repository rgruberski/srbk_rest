package com.example.srbk.model;

/**
 * Created by robert on 15.01.15.
 */
public class Ticket {

    private Integer id;
    private Seat seat;
    private TicketType type = new TicketType(1);

    public Ticket() {

    }

    public Ticket(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Seat getSeat() {
        return seat;
    }

    public void setSeat(Seat seat) {
        this.seat = seat;
    }

    public TicketType getType() {
        return type;
    }

    public void setType(TicketType type) {
        this.type = type;
    }
}
