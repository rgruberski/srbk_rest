package com.example.srbk.model;

import com.example.srbk.enumerator.ProductStatus;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by robert on 10.01.15.
 */
@Entity
@Table(name = "products")
public class Product implements Serializable {

    @Id
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL)
    private ProductCategory category;

    private ProductStatus status;

    private String name;

    @Column(length = 10000)
    private String description;

    private String picture;

    private Integer ageRestriction;

    public Product() {
    }

    public Product(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public ProductStatus getStatus() {
        return status;
    }

    public void setStatus(ProductStatus status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Integer getAgeRestriction() {
        return ageRestriction;
    }

    public void setAgeRestriction(Integer ageRestriction) {
        this.ageRestriction = ageRestriction;
    }
}
