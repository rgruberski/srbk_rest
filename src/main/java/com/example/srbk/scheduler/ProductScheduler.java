package com.example.srbk.scheduler;

import com.example.srbk.service.ProductCategoryService;
import com.example.srbk.service.ProductService;
import com.example.srbk.service.SeanceService;

import javax.ejb.*;
import javax.inject.Inject;

/**
 * Created by robert on 13.01.15.
 */
@Singleton
@TransactionManagement(TransactionManagementType.BEAN)
@ConcurrencyManagement(ConcurrencyManagementType.BEAN)
public class ProductScheduler {

    @Inject
    private ProductService productService;

    @Inject
    private ProductCategoryService productCategoryService;

    @Inject
    private SeanceService seanceService;

    @Schedule(second = "0", minute = "*", hour = "*")
    public void test() {

        try {
            //productCategoryService.importFromWebService();
            productService.importFromWebService();
            seanceService.importFromWebService();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
