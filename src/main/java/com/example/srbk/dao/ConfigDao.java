package com.example.srbk.dao;

import com.example.srbk.model.Config;

import java.util.Date;

/**
 * Created by mateusz on 14.01.15.
 */
public interface ConfigDao extends GenericDao<Config> {
    public Config getConfigByParameter(String parameter);

    public Date getDate(String parameter) throws Exception;
}