package com.example.srbk.dao;

import com.example.srbk.model.Product;
import com.example.srbk.model.Seance;

import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
public interface SeanceDao extends GenericDao<Seance> {
    public List<Seance> findProductSeances(Product product);
}
