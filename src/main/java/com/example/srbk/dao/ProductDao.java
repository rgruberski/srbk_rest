package com.example.srbk.dao;

import com.example.srbk.model.Product;

/**
 * Created by mateusz on 11.01.15.
 */
public interface ProductDao extends GenericDao<Product> {

}