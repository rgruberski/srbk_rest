package com.example.srbk.dao.impl;

import com.example.srbk.dao.ConfigDao;
import com.example.srbk.model.Config;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;

/**
 * Created by mateusz on 14.01.15.
 */
@Named
@RequestScoped
public class ConfigDaoImpl extends GenericDaoImpl<Config> implements ConfigDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Config.class);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public Config getConfigByParameter(String parameter) {

        final CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        final CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Config.class);

        final Root configRoot = criteriaQuery.from(Config.class);

        criteriaQuery.where(criteriaBuilder.and(criteriaBuilder.equal(configRoot.get("parameter"), parameter)));

        final TypedQuery query = em.createQuery(criteriaQuery);

        try {
            return (Config) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    @Override
    public Date getDate(String parameter) throws Exception {

        Config config = getConfigByParameter(parameter);

        if (config != null) {
            return config.getDateValue();
        }

        throw new Exception();
    }
}
