package com.example.srbk.dao.impl;

import com.example.srbk.dao.ProductDao;
import com.example.srbk.model.Product;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Created by mateusz on 11.01.15.
 */
@Named
@RequestScoped
public class ProductDaoImpl extends GenericDaoImpl<Product> implements ProductDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Product.class);
    }
}
