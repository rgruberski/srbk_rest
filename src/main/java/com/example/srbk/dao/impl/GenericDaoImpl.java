package com.example.srbk.dao.impl;

import com.example.srbk.dao.GenericDao;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaQuery;
import javax.transaction.UserTransaction;
import java.util.List;

/**
 * Created by mateusz on 11.01.15.
 */
public abstract class GenericDaoImpl<T> implements GenericDao<T> {


    @PersistenceContext(unitName = "srbkDS")
    protected EntityManager em;
    protected Class<T> entityClass;
    @Resource
    private UserTransaction utx;

    @Override
    public T find(int id) {
        return em.find(entityClass, id);
    }

    public void save(T entity) {
        em.persist(entity);
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<T> findAll() {
        CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
        cq.select(cq.from(entityClass));
        return em.createQuery(cq).getResultList();
    }

    public T update(T entity) {
        return em.merge(entity);
    }

    public void setEntityClass(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void transactionOpen() throws Exception {
        utx.begin();
    }

    @Override
    public void transactionCommit() throws Exception {
        try {
            utx.commit();
        } catch (Exception e) {
            utx.rollback();
            throw e;
        }
    }
}