package com.example.srbk.dao.impl;

import com.example.srbk.dao.ProductCategoryDao;
import com.example.srbk.model.ProductCategory;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * Created by mateusz on 14.01.15.
 */
@Named
@RequestScoped
public class ProductCategoryDaoImpl extends GenericDaoImpl<ProductCategory> implements ProductCategoryDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(ProductCategory.class);
    }
}
