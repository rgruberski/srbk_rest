package com.example.srbk.dao.impl;

import com.example.srbk.dao.SeanceDao;
import com.example.srbk.model.Product;
import com.example.srbk.model.Seance;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by mateusz on 14.01.15.
 */
@Named
@RequestScoped
public class SeanceDaoImpl extends GenericDaoImpl<Seance> implements SeanceDao {

    @PostConstruct
    public void init() {
        super.setEntityClass(Seance.class);
    }

    @Override
    @SuppressWarnings({"unchecked", "rawtypes"})
    public List<Seance> findProductSeances(Product product) {

        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery cq = cb.createQuery();
        Root c = cq.from(entityClass);
        cq.select(cb.construct(entityClass, c.get("id"), c.get("hall"), c.get("startTime")));
        cq.where(cb.equal(c.get("product"), product));
        return em.createQuery(cq).getResultList();
    }
}
