package com.example.srbk.dao;

import java.util.List;

/**
 * Created by mateusz on 11.01.15.
 */
public interface GenericDao<T> {
    public T find(int id);

    public List<T> findAll();

    public void save(T entity);

    public T update(T entity);

    public void transactionOpen() throws Exception;

    public void transactionCommit() throws Exception;
}