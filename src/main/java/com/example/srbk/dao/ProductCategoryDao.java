package com.example.srbk.dao;

import com.example.srbk.model.ProductCategory;

/**
 * Created by mateusz on 14.01.15.
 */
public interface ProductCategoryDao extends GenericDao<ProductCategory> {
}
